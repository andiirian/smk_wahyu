let database = require('../database/Database')
let passwordHash = require('password-hash')
let session_store
let error = null
module.exports = {
    getLogin : (req, res, next) =>{
        res.render('guru/login',{
            error : error
        })
        error = null
    },
    postLogin : (req, res, next) => {
        session_store = req.session
        let user = req.body.username
        let password = req.body.password
        
       
        database.selectWhere('tbl_guru', '*', {email : user})
            .then( async result => {
                if (result.length != 0) {
                    if (passwordHash.verify(password, result[0].password)) {
                        let idSemester = await database.selectWhere('tbl_semester', '*', {status : 1})
                        
                        session_store.status = 3
                        session_store.logged_in = true
                   
                        session_store.id_semester = idSemester[0].id
                        session_store.id_guru = result[0].id
                        
                        res.redirect('/guru')
                        
                        
                    }else {
                        error = 1
                        res.redirect('/guru/login')
                    }
                   }else{
                    error = 2
                    res.redirect('/guru/login')
                   }
            })
    },
    getIndex : (req, res, next) =>{
        database.query(`SELECT j.id, j.jam_mulai,j.jam_selesai, h.nama_hari, m.nama_mapel, k.nama_kelas FROM tbl_jadwal j,
         tbl_hari h, tbl_mapel m, tbl_kelas k WHERE j.id_semester = ${session_store.id_semester} AND j.id_hari = h.id
          AND j.id_kelas = k.id AND j.id_mapel = m.id AND j.id_guru = ${session_store.id_guru}`).then(result =>{
              res.render('guru/index',{
                  result : result,
                  role : session_store.status
              })
          } )
    },
    getKelasNilai : (req, res, next) =>{
        database.query(`SELECT m.id AS id_mapel, k.id AS id_kelas, m.nama_mapel, k.nama_kelas FROM tbl_jadwal j, tbl_mapel m, tbl_kelas k
         WHERE j.id_semester =  ${session_store.id_semester} AND j.id_kelas = k.id AND j.id_mapel = m.id AND j.id_guru = ${session_store.id_guru}`)
         .then(result => {
             res.render('guru/table-nilai', {
                 result : result,
                 role : session_store.status
             })
         })


    },
    getNilaiKelas : (req, res, next) =>{
        database.query(`SELECT s.id, s.nama FROM tbl_siswa s, tbl_kelas_siswa ks
         WHERE s.id = ks.id_siswa AND ks.id_kelas = ${req.params.id_kelas} AND ks.status_kelas = 1`)
            .then(result => {
                res.render('guru/table-pemberian-nilai',{
                    result : result,
                    role : session_store.status
                })
            })
    },
    postNilaiKelas : (req, res, next) => {
       let id_siswa = req.body.id
       let id_kelas = req.params.id_kelas
       let id_mapel = req.params.id_mapel

       for (let index = 0; index < id_siswa.length; index++) {
           let data = {
               id_siswa : id_siswa[index],
               id_guru : session_store.id_guru,
               id_semester : session_store.id_semester,
               id_mapel : id_mapel,
               id_kelas : id_kelas,
               n_tugas : req.body.nt[index],
               n_uts : req.body.nu[index],
               n_uas : req.body.nuas[index]

           }

           database.insert('tbl_nilai', data)
           
       }
       res.redirect('/guru/table-kelas-nilai')

    },
    getTableKelasKehadiran : (req, res, next) => {
        database.query(`SELECT m.id AS id_mapel, k.id AS id_kelas, m.nama_mapel, k.nama_kelas FROM tbl_jadwal j, tbl_mapel m, tbl_kelas k
         WHERE j.id_semester = ${session_store.id_semester} AND j.id_kelas = k.id AND j.id_mapel = m.id AND j.id_guru = ${session_store.id_guru}`)
         .then(result => {
             res.render('guru/table-kelas-kehadiran', {
                 result : result,
                 role : session_store.status
             })
         })
    },
    getKehadiranSiswa : (req, res, next) =>{
        database.query(`SELECT s.id, s.nama FROM tbl_siswa s, tbl_kelas_siswa ks
         WHERE s.id = ks.id_siswa AND ks.id_kelas = ${req.params.id_kelas} AND ks.status_kelas = 1`)
            .then(result => {
                res.render('guru/table-pemberian-kehadiran',{
                    result : result,
                    role : session_store.status
                })
            })
    },
    postKehadiranSiswa :  (req, res, next) => {
        let id_siswa = req.body.id
        let id_kelas = req.params.id_kelas
        let id_mapel = req.params.id_mapel
 
        for (let index = 0; index < id_siswa.length; index++) {
            let data = {
                id_siswa : id_siswa[index],
                id_guru : session_store.id_guru,
                id_semester :session_store.id_semester,
                id_mapel : id_mapel,
                id_kelas : id_kelas,
                alfa : req.body.alfa[index],
                izin : req.body.izin[index],
                hadir : req.body.hadir[index]
 
            }
 
            database.insert('tbl_kehadiran', data)
            
        }
        res.redirect('/guru/table-kelas-kehadiran')
 
     },
     getLogout: (req,res, next) =>{
        req.session.destroy(function(err) {
            if (err) {
                throw err
            }
            res.redirect('/guru/login')
          })
    },
    getChangePassword : (req, res, next) => {
        res.render('guru/form-password',{
            role : session_store.status,
            error : error
        }) 
        error = null
    },
    postChangePassword : (req, res, next) =>{
        let password = passwordHash.generate(req.body.password)

        

        database.update('tbl_guru',{password : password},{id : session_store.id_guru})
            .then(result =>{
                if (result) {
                    error = 2
                    res.redirect('/guru/change.password')
                } else {
                    error = 1
                    res.redirect('/guru/change.password')
                }
            })  
    },
}