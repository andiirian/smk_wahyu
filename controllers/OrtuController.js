let database = require('../database/Database')
let passwordHash = require('password-hash')
let session_store

let error = null;
module.exports = {
    getLogin : (req, res, next) => {
        res.render('ortu/login',{
            error : error
        })
        error = null
    },
    postLogin :(req, res, next) => {
        session_store = req.session
        let user = req.body.username
        let password = req.body.password
        
       
        database.selectWhere('tbl_ortu', '*', {email : user})
            .then( async result => {
                if (result.length != 0) {
                    if (passwordHash.verify(password, result[0].password)) {
                        let idSemester = await database.selectWhere('tbl_semester', '*', {status : 1})
                        let idKelas = await database.selectWhereAnd('tbl_kelas_siswa','*', [
                        {
                            id_siswa : result[0].id
                        },
                        {
                            status_kelas : 1
                        }
                        ])
                        session_store.status = 4
                        session_store.logged_in = true
                        session_store.id_kelas = idKelas[0].id_kelas
                        session_store.id_semester = idSemester[0].id
                        session_store.id_siswa = result[0].id_siswa
                        session_store.id_user = result[0].id
                        
                        res.redirect('/ortu')
                        
                        
                    }else {
                        error = 1
                        res.redirect('/ortu/login')
                    }
                   }else{
                    error = 2
                    res.redirect('/ortu/login')
                   }
            })
    },
    getIndex : (req, res, next) => {
        database.query(`SELECT j.*, h.nama_hari, m.nama_mapel, k.nama_kelas FROM tbl_hari h, tbl_jadwal j, tbl_kelas k, tbl_mapel m
         WHERE j.id_hari = h.id AND j.id_mapel = m.id AND id_semester = ${session_store.id_semester} AND j.id_kelas = ${session_store.id_kelas} AND j.id_kelas = k.id`)
            .then(result => {
                res.render('ortu/index',{
                    result : result,
                    role    : session_store.status
                })
            })
    },
    getTableKelasNilai : (req, res, next) =>{
        database.query(`SELECT j.*, k.nama_kelas FROM tbl_kelas_siswa j, tbl_kelas k
         WHERE j.id_siswa = ${session_store.id_siswa} AND j.id_kelas = k.id`)
         .then(result => {
             res.render('ortu/table-nilai-kelas', {
                 result : result,
                 role    : session_store.status
             })
         })
    },
    getTableNilai : async (req, res, next) => {
       let dataSemester1 = await database.query(`SELECT g.nama, m.nama_mapel, n.* FROM tbl_guru g, tbl_mapel m, tbl_nilai n
         WHERE n.id_siswa = ${session_store.id_siswa} AND n.id_mapel = m.id AND n.id_guru = g.id AND n.id_kelas = ${req.params.id} 
         AND n.id_semester = 1`)
       let dataSemester2 = await database.query(`SELECT g.nama, m.nama_mapel, n.* FROM tbl_guru g, tbl_mapel m, tbl_nilai n
       WHERE n.id_siswa = ${session_store.id_siswa} AND n.id_mapel = m.id AND n.id_guru = g.id AND n.id_kelas = ${req.params.id} 
       AND n.id_semester = 2`)

       res.render('ortu/table-nilai', {
           semester1 : dataSemester1,
           semester2 : dataSemester2,
           role    : session_store.status
       })
    },
    getTableKelasKehadiran : (req, res, next) =>{
        database.query(`SELECT j.*, k.nama_kelas FROM tbl_kelas_siswa j, tbl_kelas k
         WHERE j.id_siswa = ${session_store.id_siswa} AND j.id_kelas = k.id`)
         .then(result => {
             res.render('ortu/table-kehadiran-kelas', {
                 result : result,
                 role    : session_store.status
             })
         })
    },
    getTableKehadiran : async (req, res, next) => {
        let dataSemester1 = await database.query(`SELECT g.nama, m.nama_mapel, k.* FROM tbl_guru g, tbl_mapel m, tbl_kehadiran k
          WHERE k.id_siswa = ${session_store.id_siswa} AND k.id_mapel = m.id AND k.id_guru = g.id AND k.id_kelas = ${req.params.id} 
          AND k.id_semester = 1`)
        let dataSemester2 = await database.query(`SELECT g.nama, m.nama_mapel, k.* FROM tbl_guru g, tbl_mapel m, tbl_kehadiran k
        WHERE k.id_siswa = ${session_store.id_siswa} AND k.id_mapel = m.id AND k.id_guru = g.id AND k.id_kelas = ${req.params.id} 
        AND k.id_semester = 2`)
 
        res.render('ortu/table-kehadiran', {
            semester1 : dataSemester1,
            semester2 : dataSemester2,
            role    : session_store.status
        })
     },
     getInfoBeasiswa : (req, res, next) =>{
         database.query(`SELECT *, DATE_FORMAT(tgl, '%d-%m-%Y') AS date  FROM tbl_info WHERE status = 2 ORDER BY id DESC`)
            .then(result =>{
                res.render('ortu/beasiswa',{
                    role : session_store.status,
                    result : result
                })
            })
     },
     getDetailInfoBeasiswa : (req, res, next) => {
        database.selectWhere('tbl_info', `*, DATE_FORMAT(tgl, '%d-%m-%Y') AS date`, {id : req.params.id})
            .then(result => {
                res.render('ortu/beasiswa-detail',{
                    role : session_store.status,
                    data : result[0]
                })
            })
     },
     getInfoLoker : (req, res, next) =>{
        database.query(`SELECT *, DATE_FORMAT(tgl, '%d-%m-%Y') AS date  FROM tbl_info WHERE status = 1 ORDER BY id DESC`)
           .then(result =>{
               res.render('ortu/beasiswa',{
                   role : session_store.status,
                   result : result
               })
           })
    },
    getDetailInfoLoker : (req, res, next) => {
       database.selectWhere('tbl_info', `*, DATE_FORMAT(tgl, '%d-%m-%Y') AS date`, {id : req.params.id})
           .then(result => {
               res.render('ortu/beasiswa-detail',{
                   role : session_store.status,
                   data : result[0]
               })
           })
    },
    getLogout: (req,res, next) =>{
        req.session.destroy(function(err) {
            if (err) {
                throw err
            }
            res.redirect('/ortu/login')
          })
    },
    getChangePassword : (req, res, next) => {
        res.render('ortu/form-password',{
            role : session_store.status,
            error : error
        }) 
        error = null
    },
    postChangePassword : (req, res, next) =>{
        let password = passwordHash.generate(req.body.password)

        

        database.update('tbl_ortu',{password : password},{id : session_store.id_user})
            .then(result =>{
                if (result) {
                    error = 2
                    res.redirect('/ortu/change.password')
                } else {
                    error = 1
                    res.redirect('/ortu/change.password')
                }
            })  
    },
}