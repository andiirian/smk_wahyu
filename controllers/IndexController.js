let database  = require('../database/Database')

module.exports = {
    getIndex : async (req, res, next) => {
        let data = await Promise.all([
            database.selectCount('tbl_guru', 'id', 'guru'),
            database.selectCount('tbl_siswa', 'id', 'siswa'),
            database.selectCount('tbl_peminatan', 'id', 'jurusan'),
            database.selectCount('tbl_kelas', 'id', 'kelas'),

        ])
        let visimisi = await database.select('tbl_visimisi', '*')
        let KategoriSarana = await database.select('tbl_kategori_sarana', '*')
        let sarana = await database.select('tbl_sarana', '*')
        let beritaSekolah = await database.query(`SELECT *, DATE_FORMAT(tgl, '%d-%m-%Y') AS date  FROM tbl_kegiatan_sekolah WHERE status = 2 ORDER BY id DESC LIMIT 3`)
        let prestasiSekolah = await database.query(`SELECT *, DATE_FORMAT(tgl, '%d-%m-%Y') AS date  FROM tbl_kegiatan_sekolah WHERE status = 1 ORDER BY id DESC LIMIT 3`)

        let dataTeam = await  database.query('SELECT o.*, s.posisi FROM tbl_organisasi o, tbl_struktur_organisasi s WHERE o.id_struktur = s.id AND status = 1 ' )
        let profile = await database.select('tbl_profil_sekolah', '*')
        let kalender = await database.select('tbl_kalender_pendidikan', '*')
        res.render('index',{
            data : data,
            visimisi : visimisi[0],
            ks : KategoriSarana,
            sarana : sarana,
            beritaSekolah : beritaSekolah,
            prestasiSekolah : prestasiSekolah,
            dataTeam : dataTeam,
            kalender : kalender,
            profile : profile[0]
        })
    },
    getBlog : async (req, res, next) =>{
        let title;
        let id = req.params.id
        
        let data = await database.query(`SELECT *, DATE_FORMAT(tgl, '%d-%m-%Y') AS date  FROM tbl_kegiatan_sekolah WHERE status = ${id} ORDER BY id DESC`)

        if (id == 1) {
            title = `Prestasi Sekolah`
        }else if(id == 2){
            title = `Berita Sekolah`
        }
        let kalender = await database.select('tbl_kalender_pendidikan', '*')

        res.render('blog',{
            title : title,
            data : data,
            kalender : kalender
        })
        
    },
    getBlogDetail : async (req, res, next) =>{
        let data = await database.selectWhere('tbl_kegiatan_sekolah', `*, DATE_FORMAT(tgl, '%d-%m-%Y') AS date`, {id : req.params.id})
        let relatedPost = await database.query(`SELECT *, DATE_FORMAT(tgl, '%d-%m-%Y') AS date  FROM tbl_kegiatan_sekolah WHERE status = ${data[0].status} AND id != ${req.params.id} ORDER BY id DESC LIMIT 3`)
        let kalender = await database.select('tbl_kalender_pendidikan', '*')
        res.render('blog-detail',{
            data : data[0],
            relatedPost : relatedPost,
            kalender : kalender
        })
    },
    getJadwal : (req, res, next) =>{
        database.selectWhere('tbl_kalender_pendidikan', `*, DATE_FORMAT(tgl, '%Y/%m-%d') AS date`, {id : req.params.id})
            .then(result =>{
                res.render('jadwal',{
                    result : result[0]
                })
            })
    }
}