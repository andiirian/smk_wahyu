let database = require('../database/Database')
let passwordHash = require('password-hash')
let session_store

let error = null;
module.exports = {
    getLogin : (req, res, next) => {
        res.render('siswa/login',{
            error : error
        })
        error = null
    },
    postLogin :(req, res, next) => {
        session_store = req.session
        let user = req.body.username
        let password = req.body.password
        
       
        database.selectWhere('tbl_siswa', '*', {email : user})
            .then( async result => {
                if (result.length != 0) {
                    if (passwordHash.verify(password, result[0].password)) {
                        let idSemester = await database.selectWhere('tbl_semester', '*', {status : 1})
                        let idKelas = await database.selectWhereAnd('tbl_kelas_siswa','*', [
                        {
                            id_siswa : result[0].id
                        },
                        {
                            status_kelas : 1
                        }
                        ])
                        session_store.status = 2
                        session_store.logged_in = true
                        session_store.id_kelas = idKelas[0].id_kelas
                        session_store.id_semester = idSemester[0].id
                        session_store.id_siswa = result[0].id
                        
                        res.redirect('/siswa')
                        
                        
                    }else {
                        error = 1
                        res.redirect('/siswa/login')
                    }
                   }else{
                    error = 2
                    res.redirect('/siswa/login')
                   }
            })
    },
    getIndex : (req, res, next) => {
        database.query(`SELECT j.*, h.nama_hari, m.nama_mapel, k.nama_kelas FROM tbl_hari h, tbl_jadwal j, tbl_kelas k, tbl_mapel m
         WHERE j.id_hari = h.id AND j.id_mapel = m.id AND id_semester = ${session_store.id_semester} AND j.id_kelas = ${session_store.id_kelas} AND j.id_kelas = k.id`)
            .then(result => {
                res.render('siswa/index',{
                    result : result,
                    role : session_store.status
                })
            })
    },
    getTableKelasNilai : (req, res, next) =>{
        database.query(`SELECT j.*, k.nama_kelas FROM tbl_kelas_siswa j, tbl_kelas k
         WHERE j.id_siswa = ${session_store.id_siswa} AND j.id_kelas = k.id`)
         .then(result => {
             res.render('siswa/table-nilai-kelas', {
                 result : result,
                 role : session_store.status
             })
         })
    },
    getTableNilai : async (req, res, next) => {
       let dataSemester1 = await database.query(`SELECT g.nama, m.nama_mapel, n.* FROM tbl_guru g, tbl_mapel m, tbl_nilai n
         WHERE n.id_siswa = ${session_store.id_siswa} AND n.id_mapel = m.id AND n.id_guru = g.id AND n.id_kelas = ${req.params.id} 
         AND n.id_semester = 1`)
       let dataSemester2 = await database.query(`SELECT g.nama, m.nama_mapel, n.* FROM tbl_guru g, tbl_mapel m, tbl_nilai n
       WHERE n.id_siswa = ${session_store.id_siswa} AND n.id_mapel = m.id AND n.id_guru = g.id AND n.id_kelas = ${req.params.id} 
       AND n.id_semester = 2`)

       res.render('siswa/table-nilai', {
           semester1 : dataSemester1,
           semester2 : dataSemester2,
           role : session_store.status
       })
    },
    getTableKelasKehadiran : (req, res, next) =>{
        database.query(`SELECT j.*, k.nama_kelas FROM tbl_kelas_siswa j, tbl_kelas k
         WHERE j.id_siswa = ${session_store.id_siswa} AND j.id_kelas = k.id`)
         .then(result => {
             res.render('siswa/table-kehadiran-kelas', {
                 result : result,
                 role : session_store.status
             })
         })
    },
    getTableKehadiran : async (req, res, next) => {
        let dataSemester1 = await database.query(`SELECT g.nama, m.nama_mapel, k.* FROM tbl_guru g, tbl_mapel m, tbl_kehadiran k
          WHERE k.id_siswa = ${session_store.id_siswa} AND k.id_mapel = m.id AND k.id_guru = g.id AND k.id_kelas = ${req.params.id} 
          AND k.id_semester = 1`)
        let dataSemester2 = await database.query(`SELECT g.nama, m.nama_mapel, k.* FROM tbl_guru g, tbl_mapel m, tbl_kehadiran k
        WHERE k.id_siswa = ${session_store.id_siswa} AND k.id_mapel = m.id AND k.id_guru = g.id AND k.id_kelas = ${req.params.id} 
        AND k.id_semester = 2`)
 
        res.render('siswa/table-kehadiran', {
            semester1 : dataSemester1,
            semester2 : dataSemester2,
            role : session_store.status
        })
     },
     getLogout: (req,res, next) =>{
        req.session.destroy(function(err) {
            if (err) {
                throw err
            }
            res.redirect('/siswa/login')
          })
    },
    getChangePassword : (req, res, next) => {
        res.render('siswa/form-password',{
            role : session_store.status,
            error : error
        }) 
        error = null
    },
    postChangePassword : (req, res, next) =>{
        let password = passwordHash.generate(req.body.password)

        

        database.update('tbl_siswa',{password : password},{id : session_store.id_siswa})
            .then(result =>{
                if (result) {
                    error = 2
                    res.redirect('/siswa/change.password')
                } else {
                    error = 1
                    res.redirect('/siswa/change.password')
                }
            })  
    },
    getNilaiPkl : (req, res, next) =>{
        database.selectWhere('tbl_pkl', '*', {id_siswa : session_store.id_siswa})
            .then(result =>{
                res.render('siswa/table-nilai-pkl',{
                    role : session_store.status,
                    result : result
                })
            })
    }
}