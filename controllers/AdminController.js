let database = require('../database/Database')
let passwordHash = require('password-hash')
let session_store;
let error = null
module.exports = {
    getLogin: (req, res, next) => {
        res.render('admin/login', {
            error: error
        })
        error = null
    },
    postLogin: (req, res, next) => {
        session_store = req.session
        let data = {
            username: req.body.username,
            password: req.body.password
        }
        database.selectWhere('tbl_admin', '*', {
                username: data.username
            })
            .then(result => {
                if (result.length != 0) {
                    if (passwordHash.verify(data.password, result[0].password)) {
                        session_store.status = 1
                        session_store.logged_in = true
                        session_store.id_user = result[0].id


                        res.redirect('/users')
                    } else {
                        error = 1
                        res.redirect('/users/login')
                    }
                } else {
                    error = 2
                    res.redirect('/users/login')
                }
            })
    },
    getIndex: async (req, res, next) => {
        let data = await Promise.all([
            database.selectCount('tbl_guru', 'id', 'guru'),
            database.selectCount('tbl_siswa', 'id', 'siswa'),
            database.selectCount('tbl_peminatan', 'id', 'jurusan'),
            database.selectCount('tbl_kelas', 'id', 'kelas'),

        ])
        var options = {
            sql: 'SELECT * FROM tbl_guru g LEFT join tbl_mapel m ON g.id_mapel = m.id',
            nestTables: true
        };
        let guru = await database.query(options)


        res.render('admin/index', {
            data: data,
            guru: guru,
            role: session_store.status

        })


    },
    getFormAdmin: (req, res, next) => {
        res.render('admin/form-admin', {
            role: session_store.status
        })
    },
    postFormAdmin: (req, res, next) => {
        let data = {
            nama: req.body.nama,
            email: req.body.email,
            username: req.body.username,
            password: passwordHash.generate(req.body.password),
            role: 0

        }

        database.insert('tbl_admin', data)
            .then(result => {
                if (result.status) {
                    res.send('berhasil')
                }
                res.send('gagal')
            })
    },

    getFormMapel: (req, res, next) => {
        res.render('admin/form-mapel', {
            error: error,
            role: session_store.status
        })
        error = null
    },

    postFormMapel: (req, res, next) => {
        let data = {
            nama_mapel: req.body.nama_mapel,

        }

        database.insert('tbl_mapel', data)
            .then(result => {
                if (result.status) {
                    error = 2
                    res.redirect('/users/form-mapel')
                } else {
                    error = 1
                    res.redirect('/users/form-mapel')
                }

            })
    },
    getTableMapel: (req, res, next) => {
        database.select('tbl_mapel', '*')
            .then(result => {
                res.render('admin/table-mapel', {
                    result: result,
                    role: session_store.status
                })
            })
    },
    getEditMapel: (req, res, next) => {
        database.selectWhere('tbl_mapel', '*', {
                id: req.params.id
            })
            .then(result => {
                res.render('admin/form-edit-mapel', {
                    nama_mapel: result[0].nama_mapel,
                    role: session_store.status
                })
            })
    },
    postEditMapel: (req, res, next) => {
        database.update('tbl_mapel', {
            nama_mapel: req.body.nama_mapel
        }, {
            id: req.params.id
        }).then(result => {
            return result ? res.redirect('/users/table-mapel') : res.send('gagal')
        })
    },
    postDelMapel: (req, res, next) => {
        database.delete('tbl_mapel', {
            id: req.body.id
        }).then(result => {
            return result ? res.send(true) : res.send(false)
        })
    },

    getFormGuru: (req, res, next) => {
        database.select('tbl_mapel', "*")
            .then(result => {
                res.render('admin/form-guru', {
                    result: result,
                    role: session_store.status
                })
            })
    },

    postFormGuru: (req, res, next) => {
        let data = {
            id_mapel: req.body.id_mapel,
            nama: req.body.nama,
            nik : req.body.nik,
            email: req.body.email,
            password: passwordHash.generate(req.body.password),
            ttl: req.body.ttl,
            jk: req.body.jk
        }

        database.insert('tbl_guru', data)
            .then(result => {
                if (result.status) {
                    res.redirect('/users/form-guru')
                } else {
                    res.send('gagal')
                }
            })
    },
    getTableGuru: (req, res, next) => {
        var options = {
            sql: 'SELECT * FROM tbl_guru g LEFT join tbl_mapel m ON g.id_mapel = m.id',
            nestTables: true
        };
        database.query(options)
            .then(result => {
                res.render('admin/table-guru', {
                    result: result,
                    role: session_store.status
                })
            })
    },
    getEditFormGuru: async (req, res, next) => {
        let dataGuru = await database.selectWhere('tbl_guru', '*', {
            id: req.params.id
        })
        let dataMapel = await database.select('tbl_mapel', '*')

        res.render('admin/form-edit-guru', {
            dataGuru: dataGuru[0],
            dataMapel: dataMapel,
            role: session_store.status
        })
    },
    postEditFormGuru: (req, res, next) => {
        let data = {
            nama: req.body.nama,
            ttl: req.body.ttl,
            id_mapel: req.body.id_mapel
        }

        database.update('tbl_guru', data, {
                id: req.params.id
            })
            .then(result => {
                return result ? res.redirect('/users/table-guru') : res.send('gagal')
            })
    },
    postDelGuru: (req, res, next) => {
        database.delete('tbl_guru', {
                id: req.body.id
            })
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },

    getFormKelas: async (req, res, next) => {
        let dataGuru = await database.select('tbl_guru', '*');
        let dataPeminatan = await database.select('tbl_peminatan', '*')
        let dataKelas = await database.select('tbl_status_kelas', '*')

        res.render('admin/form-kelas', {
            dataGuru: dataGuru,
            dataPeminatan: dataPeminatan,
            dataKelas: dataKelas,
            error: error,
            role: session_store.status
        })
        error = null
    },

    postFormKelas: (req, res, next) => {
        let data = {
            nama_kelas: req.body.nama_kelas,
            id_peminatan: req.body.id_peminatan,
            id_wali_kelas: req.body.id_wali_kelas,
            kelas: req.body.id_kelas
        }

        database.insert('tbl_kelas', data)
            .then(result => {
                if (result.status) {
                    error = 2
                    res.redirect('/users/form-kelas')
                } else {
                    error = 1
                    res.redirect('/users/form-kelas')
                }
            })
    },

    getDatabaseKelas: (req, res, next) => {
        database.query(`SELECT k.*, g.nama FROM tbl_kelas k, tbl_guru g WHERE k.id_wali_kelas = g.id`)
            .then(result => {
                res.render('admin/table-kelas-e', {
                    result: result,
                    role: session_store.status
                })
            })
    },
    getFormEditKelas: async (req, res, next) => {
        let id = req.params.id
        let dataKelas = await database.selectWhere('tbl_kelas', '*', {
            id: id
        })
        let dataGuru = await database.select('tbl_guru', '*')
        let dataPeminatan = await database.select('tbl_peminatan', '*')
        let kelas = await database.select('tbl_status_kelas', '*')
        res.render('admin/form-edit-kelas', {
            data: dataKelas[0],
            dataGuru: dataGuru,
            dataPeminatan: dataPeminatan,
            dataKelas: kelas,
            role: session_store.status
        })

    },
    postFormEditKelas: (req, res, next) => {
        let data = {
            nama_kelas: req.body.nama_kelas,
            id_peminatan: req.body.id_peminatan,
            id_wali_kelas: req.body.id_wali_kelas,
            kelas: req.body.id_kelas
        }

        database.update('tbl_kelas', data, {
                id: req.params.id
            })
            .then(result => {
                return result ? res.redirect('/users/table-kelas') : res.send('gagal')
            })
    },
    postDelKelas: (req, res, next) => {
        let id = req.body.id
        database.delete('tbl_kelas', {
                id: id
            })
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },

    getFormTa: (req, res, next) => {
        res.render('admin/form-ta', {
            error: error,
            role: session_store.status
        })
        error = null
    },

    postFormTa: (req, res, next) => {
        let data = {

            tahun_ajaran: req.body.tahun_ajaran
        }

        database.insert('tbl_tahun_ajaran', data)
            .then(result => {
                if (result.status) {
                    error = 2
                    res.redirect('/users/form-ta')
                } else {
                    error = 1
                    res.redirect('/users/form-ta')
                }
            })
    },
    getTableTa: (req, res, next) => {
        database.select('tbl_tahun_ajaran', '*')
            .then(result => {
                res.render('admin/table-ta', {
                    result: result,
                    role: session_store.status
                })
            })
    },
    getEditFormTa: (req, res, next) => {
        database.selectWhere('tbl_tahun_ajaran', '*', {
                id: req.params.id
            })
            .then(result => {
                res.render('admin/form-edit-ta', {
                    result: result[0],
                    role: session_store.status
                })
            })
    },
    postEditFormTa: (req, res, next) => {
        database.update('tbl_tahun_ajaran', {
            tahun_ajaran: req.body.tahun_ajaran
        }, {
            id: req.params.id
        }).then(result => {
            return result ? res.redirect('/users/table-ta') : res.send('gagal')
        })
    },
    postDelTa: (req, res, next) => {
        database.delete('tbl_tahun_ajaran', {
                id: req.body.id
            })
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },

    getFormSiswa: async (req, res, next) => {
        let dataKelas = await database.select('tbl_kelas', '*')
        let dataTa = await database.select('tbl_tahun_ajaran', '*')

        res.render('admin/form-siswa', {
            dataKelas: dataKelas,
            dataTa: dataTa,
            error: error,
            role: session_store.status
        })
        error = null

    },

    postFormSiswa: async (req, res, next) => {
        let dataSiswa = {
            nis: '0',
            nama: req.body.nama,
            email: req.body.email,
            password: passwordHash.generate(req.body.password),
            jk: req.body.jk,
            ttl: req.body.ttl,
            nis : req.body.nis
        }

        let id = await database.insert('tbl_siswa', dataSiswa)

        let dataOrtu = {
            id_siswa: id.result.insertId,
            nama: req.body.nama_ortu,
            email: req.body.email_ortu,
            password: passwordHash.generate(req.body.password_ortu),
            ttl: req.body.ttl_ortu,
            jk: req.body.jk_ortu
        }

        let dataKelas = {
            id_kelas: req.body.id_kelas,
            id_siswa: id.result.insertId,
            id_ta: req.body.id_ta,
            status_kelas: 1,
            status_tamat: 0
        }
        let idKelas = await database.insert('tbl_kelas_siswa', dataKelas)
        let idOrtu = await database.insert('tbl_ortu', dataOrtu)

        if (id.status == true && idOrtu.status == true && idKelas.status == true) {
            error = 2
            res.redirect('/users/form-siswa')
        } else {
            error = 1
            res.redirect('/users/form-siswa')
        }

    },
    getTableSiswa: (req, res, next) => {
        database.query(`SELECT s.*, k.nama_kelas FROM tbl_siswa s, tbl_kelas k, tbl_kelas_siswa ks
         WHERE s.id = ks.id_siswa AND k.id = ks.id_kelas AND ks.status_kelas = 1 AND status_tamat = 0`)
            .then(result => {
                res.render('admin/table-siswa', {
                    result: result,
                    role: session_store.status
                })
            })
    },
    getTableSiswaTamat : (req, res, next) =>{
        database.query(`SELECT s.*, ta.tahun_ajaran FROM tbl_siswa s, tbl_tahun_ajaran ta, tbl_kelas_siswa ks
         WHERE s.id = ks.id_siswa AND ks.status_tamat = 1 AND ks.id_ta = ta.id`)
           .then(result => {
               
               res.render('admin/table-siswa-tamat', {
                   result: result,
                   role: session_store.status
               })
           })
    },
    getEditFormSiswa: (req, res, next) => {
        let id = req.params.id

        database.query(`SELECT s.*, o.nama AS nama_ortu, o.email as email_ortu FROM tbl_siswa s, tbl_ortu o 
        WHERE s.id = ${id} AND s.id = o.id_siswa`)
            .then(result => {
                res.render('admin/form-edit-siswa', {
                    data: result[0],
                    role: session_store.status
                })
            })
    },
    postEditFormSiswa: (req, res, next) => {
        let id = req.body.id;
        let id_Siswa = req.params.id
        if (id == 1) {
            let data = {
                nama: req.body.nama,
                email: req.body.email,
                ttl: req.body.ttl,
                password: passwordHash.generate(req.body.password)

            }
            database.update('tbl_siswa', data, {
                    id: id_Siswa
                })
                .then(result => {
                    return result ? res.redirect('/users/table-siswa') : res.send('gagal')
                })
        } else if (id == 2) {
            let data = {
                nama: req.body.nama_ortu,
                email: req.body.email_ortu,
                password: passwordHash.generate(req.body.password_ortu)
            }
            database.update('tbl_ortu', data, {
                    id_siswa: id_Siswa
                })
                .then(result => {
                    return result ? res.redirect('/users/table-siswa') : res.send('gagal')
                })

        }

    },
    postDelSiswa: (req, res, next) => {
        let id = req.body.id

        database.delete('tbl_siswa', {
                id: id
            })
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },
    getTableKur: (req, res, next) => {
        database.select('tbl_semester', '*')
            .then(result => {
                res.render('admin/table-kurikulum', {
                    result: result,
                    role: session_store.status
                })
            })
    },

    getTableKelas: (req, res, next) => {
        database.select('tbl_kelas', '*')
            .then(result => {
                res.render('admin/table-kelas', {
                    result: result,
                    id: req.params.id,
                    role: session_store.status
                })
            })
    },
    getTableKurKelas: (req, res, next) => {
        database.query(`SELECT j.id, j.jam_mulai,j.jam_selesai, h.nama_hari, 
         m.nama_mapel, g.nama FROM tbl_jadwal j, tbl_hari h, tbl_guru g, tbl_mapel m 
        WHERE (j.id_semester = ${req.params.id} AND j.id_kelas = ${req.params.kelas})
         AND j.id_hari = h.id AND j.id_mapel = m.id AND j.id_guru = g.id`)
            .then(result => {
                res.render('admin/table-kurikulum-kelas', {
                    result: result,
                    id_kelas: req.params.kelas,
                    id_semester: req.params.id,
                    role: session_store.status
                })
            })
    },
    getFormKurikulum: async (req, res, next) => {
        let result = await Promise.all([
            database.select('tbl_hari', '*'),
            database.select('tbl_mapel', '*'),
            database.select('tbl_guru', '*')

        ])

        res.render('admin/form-kurikulum', {
            result: result,
            role: session_store.status
        })
    },
    getEditFormKurikulum: async (req, res, next) => {
        let result = await Promise.all([
            database.select('tbl_hari', '*'),
            database.select('tbl_mapel', '*'),
            database.select('tbl_guru', '*')

        ])
        let data = await database.selectWhere('tbl_jadwal', '*', {
            id: req.params.id
        })

        res.render('admin/form-edit-kurikulum', {
            result: result,
            data: data[0],
            role: session_store.status
        })
    },
    postEditFormKurikulum: (req, res, next) => {
        let data = {
            id_hari: req.body.id_hari,
            id_mapel: req.body.id_mapel,
            id_guru: req.body.id_guru,
            jam_mulai: req.body.jam_mulai,
            jam_selesai: req.body.jam_selesai
        }
        let id = {
            ids: req.params.id_semester,
            idk: req.params.kelas,

        }

        database.update('tbl_jadwal', data, {
                id: req.params.id
            })
            .then(result => {
                return result ? res.redirect(`/users/table-kurikulum-kelas/${id.ids}/${id.idk}`) : res.send('gagal')
            })
    },
    postDelKurikulum: (req, res, next) => {
        let id = req.body.id
        database.delete('tbl_jadwal', {
                id: id
            })
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },
    postFormKurikulum: (req, res, next) => {
        let data = {
            id_hari: req.body.id_hari,
            id_kelas: req.params.kelas,
            id_mapel: req.body.id_mapel,
            id_guru: req.body.id_guru,
            id_semester: req.params.id,
            jam_mulai: req.body.jam_mulai,
            jam_selesai: req.body.jam_selesai,

        }

        database.insert('tbl_jadwal', data)
            .then(result => {
                return result.status ? res.redirect(`/users/table-kurikulum-kelas/${req.params.id}/${req.params.kelas}`) :
                    res.send(result)
            })
    },

    postSelectGuru : (req, res, next) =>{
        database.selectWhere('tbl_guru', '*', {id_mapel : req.body.id})
            .then(result =>{
                if (result.length != 0) {
                    res.send(result)
                } else {
                    res.send(false)                    
                }
            })
    },
    getTableKelasSiswa: async (req, res, next) => {
        let dataKelas = await database.select('tbl_kelas', '*')
        let dataSiswa = await database.query('SELECT k.*, s.nama FROM tbl_kelas_siswa k, tbl_siswa s WHERE k.status_kelas = 1 AND k.id_siswa = s.id')

        function data() {
            let data = [];
            let data1 = []

            dataKelas.forEach(function (item, index) {
                dataSiswa.forEach(function (item1, indexx) {
                    if (item.id == item1.id_kelas && item1.status_kelas == 1) {
                        data1.push(item1)
                    }
                })
                item.daftarSiswa = {}
                item.daftarSiswa = data1
                data.push({
                    data: item
                })
                data1 = [];
            });

            return data
        }

        res.render('admin/table-kelas-siswa', {
            data: data(),
            role: session_store.status
        })



    },

    postTableKelasSiswa: async (req, res, next) => {
        let data = req.body.tes;
        let dataKelas = req.body.kelas
        let statusKelas = req.body.statusKelas



        if (dataKelas == 0) {
           
            let dataTa = await database.selectAllDescLimit('tbl_tahun_ajaran', 'id', 1)
            if (Array.isArray(data)) {

                data.forEach(async function (item) {
                    let split = item.split(' ')

                    let data1 = {
                        id_kelas: null,
                        id_siswa: split[1],
                        id_ta: dataTa[0].id,
                        status_kelas: 4,
                        status_tamat: 1
                    }
                    database.insert('tbl_kelas_siswa', data1)
                    database.update('tbl_kelas_siswa', {
                        status_kelas: 0
                    }, {
                        id: split[0]
                    })

                })

            } else {
                let split = data.split(' ')

                let data1 = {
                    id_kelas: null,
                    id_siswa: split[1],
                    id_ta: dataTa[0].id,
                    status_kelas: 4,
                    status_tamat: 1
                }
                database.insert('tbl_kelas_siswa', data1)
               
                database.update('tbl_kelas_siswa', {
                    status_kelas: 0
                }, {
                    id: split[0]
                })

            }
        } else {
            if (statusKelas[0] == 2) {
                if (Array.isArray(data)) {

                    data.forEach(async function (item) {
                        let split = item.split(' ')
                        let data1 = {
                            id_kelas: dataKelas,
                            id_siswa: split[1],
                            id_ta: split[2],
                            status_kelas: split[3],
                            status_tamat: 0
                        }
                        let data2 = {
                            id_siswa: split[1],
                            id_kelas: dataKelas,
                            status: 0
                        }
                        database.insert('tbl_pkl', data2)
                        database.insert('tbl_kelas_siswa', data1)
                        database.update('tbl_kelas_siswa', {
                            status_kelas: 0
                        }, {
                            id: split[0]
                        })

                    })

                } else {
                    let split = data.split(' ')
                    let data1 = {
                        id_kelas: dataKelas,
                        id_siswa: split[1],
                        id_ta: split[2],
                        status_kelas: split[3],
                        status_tamat: 0
                    }
                    let data2 = {
                        id_siswa: split[1],
                        id_kelas: dataKelas,
                        status: 0
                    }
                    database.insert('tbl_pkl', data2)

                    database.insert('tbl_kelas_siswa', data1)
                    database.update('tbl_kelas_siswa', {
                        status_kelas: 0
                    }, {
                        id: split[0]
                    })

                }


            } else {
                if (Array.isArray(data)) {

                    data.forEach(async function (item) {
                        let split = item.split(' ')
                        let data1 = {
                            id_kelas: dataKelas,
                            id_siswa: split[1],
                            id_ta: split[2],
                            status_kelas: split[3],
                            status_tamat: 0
                        }
                        database.insert('tbl_kelas_siswa', data1)
                        database.update('tbl_kelas_siswa', {
                            status_kelas: 0
                        }, {
                            id: split[0]
                        })

                    })

                } else {
                    let split = data.split(' ')
                    let data1 = {
                        id_kelas: dataKelas,
                        id_siswa: split[1],
                        id_ta: split[2],
                        status_kelas: split[3],
                        status_tamat: 0
                    }

                    database.insert('tbl_kelas_siswa', data1)
                    database.update('tbl_kelas_siswa', {
                        status_kelas: 0
                    }, {
                        id: split[0]
                    })

                }



            }

        }
        res.redirect('/users/table-kelas-siswa')
    },

    getTablePkl: async (req, res, next) => {
      let data1 = await  database.query(`SELECT p.*,s.nama, k.nama_kelas FROM tbl_siswa s, tbl_kelas k, tbl_pkl p
         WHERE p.id_siswa = s.id AND p.id_kelas = k.id AND p.status= 0`)
      let data2 = await database.query(`SELECT p.*,s.nama, k.nama_kelas FROM tbl_siswa s, tbl_kelas k, tbl_pkl p
      WHERE p.id_siswa = s.id AND p.id_kelas = k.id AND p.status= 1`)
            res.render('admin/table-pkl', {
                data1: data1,
                data2 : data2,
                role: session_store.status
            })

    },

    getFormPkl: (req, res, next) => {
        res.render('admin/form-pkl', {
            role: session_store.status
        })
    },

    postFormPkl: (req, res, next) => {
        let id = req.params.id
        database.update('tbl_pkl', {
            tempat_pkl: req.body.tempat,
            nilai : req.body.nilai,
            status: 1
        }, {
            id: id
        }).then(result => {
            return result ? res.redirect('/users/table-pkl') : res.send('gagal')
        })
    },
    getFormVisi: (req, res, next) => {
        database.select('tbl_visimisi', '*')
            .then(result => {
                res.render('admin/form-visimisi', {
                    role: session_store.status,
                    data: result,
                    error: error
                })
                error = null
            })
    },
    postFormVisi: (req, res, next) => {
        let data = {
            visi: req.body.visi,
            misi: req.body.misi
        }

        database.insert('tbl_visimisi', data)
            .then(result => {
                if (result.status) {
                    error = 2
                    res.redirect('/form-visimisi')
                } else {
                    error = 1
                    res.redirect('/users//form-visimisi')
                }
            })
    },
    getEditFormVisi: (req, res, next) => {
        database.selectWhere('tbl_visimisi', '*', {
                id: req.params.id
            })
            .then(result => {
                res.render('admin/form-edit-visi', {
                    role: session_store.status,
                    data: result[0],
                    error: error
                })
                error = null
            })
    },
    postEditFormVisi: (req, res, next) => {
        let data = {
            visi: req.body.visi,
            misi: req.body.misi
        }
        database.update('tbl_visimisi', data, {
                id: req.params.id
            })
            .then(result => {
                if (result) {
                    error = 2
                    res.redirect(`/users/edit.visimisi/${req.params.id}`)
                } else {
                    error = 1
                    res.redirect(`/users/edit.visimisi/${req.params.id}`)
                }
            })
    },
    getFormProfil: (req, res, next) => {
        database.select('tbl_profil_sekolah', '*')
            .then(result => {
                res.render('admin/form-profil-sekolah', {
                    role: session_store.status,
                    data: result,
                    error: error
                })
                error = null
            })
    },
    postFormProfil: (req, res, next) => {
        database.insert('tbl_profil_sekolah', {
                deskripsi: req.body.deskripsi
            })
            .then(result => {
                if (result.status) {
                    error = 2
                    res.redirect('/users/form-profil-sekolah')
                } else {
                    error = 1
                    res.redirect('/users/form-profil-sekolah')
                }
            })
    },
    getEditFormProfil: (req, res, next) => {
        database.selectWhere('tbl_profil_sekolah', '*', {
                id: req.params.id
            })
            .then(result => {
                res.render('admin/form-edit-profil-sekolah', {
                    role: session_store.status,
                    data: result[0],
                    error: error
                })
                error = null
            })
    },
    postEditFormProfil: (req, res, next) => {
        database.update('tbl_profil_sekolah', {
            deskripsi: req.body.deskripsi
        }, {
            id: req.params.id
        }).then(result => {
            if (result) {
                error = 2
                res.redirect(`/users/edit.profilsekolah/${req.params.id}`)
            } else {
                error = 1
                res.redirect(`/edit.profilsekolah/${req.params.id}`)
            }
        })
    },
    getFormKalender: (req, res, next) => {
        res.render('admin/form-kalender-pendidikan', {
            role: session_store.status,
            error: error
        })
        error = null
    },
    postFormKalender: (req, res, next) => {
        let tgl = req.body.tgl.split('/')

        let data = {
            title: req.body.title,
            tgl: `${tgl[2]}-${tgl[0]}-${tgl[1]}`
        }

        database.insert('tbl_kalender_pendidikan', data)
            .then(result => {
                if (result.status) {
                    error = 2
                    res.redirect('/users/form-kalender-pendidikan')
                } else {
                    error = 1
                    res.redirect('/users/form-kalender-pendidikan')
                }
            })
    },
    getFormEditKalender: (req, res, next) => {
        database.selectWhere('tbl_kalender_pendidikan', `*, DATE_FORMAT(tgl, '%m/%d/%Y') AS date`, {
                id: req.params.id
            })
            .then(result => {

                res.render('admin/form-edit-kalender-pendidikan', {
                    data: result[0],
                    role: session_store.status,

                })
            })
    },
    postFormEditKalender: (req, res, next) => {
        let tgl = req.body.tgl.split('/')

        let data = {
            title: req.body.title,
            tgl: `${tgl[2]}-${tgl[0]}-${tgl[1]}`
        }

        database.update('tbl_kalender_pendidikan', data, {
                id: req.params.id
            })
            .then(result => {
                if (result) {
                    res.redirect('/users/table-kalender-pendidikan')
                } else {
                    res.redirect('/users/table-kalender-pendidikan')
                }
            })
    },
    postDelKalender: (req, res, next) => {
        database.delete('tbl_kalender_pendidikan', {
                id: req.body.id
            })
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },
    getTableKalender: (req, res, next) => {
        database.select('tbl_kalender_pendidikan', `*, DATE_FORMAT(tgl, '%d-%m-%Y') AS date`)
            .then(result => {

                res.render('admin/table-kalender-pendidikan', {
                    role: session_store.status,
                    result: result
                })
            })
    },
    getFormKategoriSarana: (req, res, next) => {
        res.render('admin/form-kategori-sarana', {
            role: session_store.status,
            error: error
        })
        error = null
    },
    postFormKategoriSarana: async (req, res, next) => {
        let data = {
            kategori: req.body.kategori,
            deskripsi: req.body.deskripsi
        }

        let id = await database.insert('tbl_kategori_sarana', data)

        req.files.forEach(async function (data) {
            database.insert('tbl_sarana', {
                id_kategori: id.result.insertId,
                foto: `/admin/images/foto-sarana/${data.filename}`
            })
        })

        error = 2
        res.redirect('/users/form-kategori-sarana')

    },
    getEditKategoriSarana: (req, res, next) => {
        database.selectWhere('tbl_kategori_sarana', '*', {
                id: req.params.id
            })
            .then(result => {
                res.render('admin/form-edit-kategori-sarana', {
                    data: result[0],
                    role: session_store.status
                })
            })
    },
    postEditKategoriSarana: (req, res, next) => {
        database.update('tbl_kategori_sarana', {
            kategori: req.body.kategori
        }, {
            id: req.params.id
        }).then(result => {
            res.redirect('/users/table-kategori-sarana')
        })
    },
    postDelKategoriSarana: (req, res, next) => {
        database.delete('tbl_kategori_sarana', {
                id: req.body.id
            })
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },
    getTableKategoriSarana: (req, res, next) => {
        database.select('tbl_kategori_sarana', '*')
            .then(result => {
                res.render('admin/table-kategori-sarana', {
                    result: result,
                    role: session_store.status
                })
            })
    },
    getFotoSarana: (req, res, next) => {
        database.selectWhere('tbl_sarana', '*', {
                id_kategori: req.params.id
            })
            .then(result => {
                res.render('admin/table-foto-sarana', {
                    role: session_store.status,
                    result: result,
                    id_kategori: req.params.id
                })
            })
    },
    delFotoSarana: (req, res, next) => {
        database.delete('tbl_sarana', {
                id: req.body.id
            })
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },
    getFormFotoSarana: (req, res, next) => {
        res.render('admin/form-foto-sarana', {
            role: session_store.status
        })
    },
    postFotoSarana: (req, res, next) => {
        req.files.forEach(async function (data) {
            database.insert('tbl_sarana', {
                id_kategori: req.params.id,
                foto: `/admin/images/foto-sarana/${data.filename}`
            })
        })

        res.redirect(`/users/lihat.fotosarana/${req.params.id}`)
    },
    getInfoLoker: (req, res, next) => {
        res.render('admin/form-info-loker', {
            role: session_store.status,
            error: error
        })
        error = null
    },
    postInfoloker: (req, res, next) => {
        let data = {
            title: req.body.title,
            deskripsi: req.body.deskripsi,
            status: 1,
            foto: `/admin/images/foto-loker/${req.file.filename}`
        }

        database.insert('tbl_info', data)
            .then(result => {
                if (result.status) {
                    error = 2
                    res.redirect('/users/form-info-loker')
                } else {
                    error = 1
                    res.redirect('/users/form-info-loker')
                }
            })
    },
    getTableLoker: (req, res, next) => {
        database.selectWhere('tbl_info', '*', {
                status: 1
            })
            .then(result => {
                res.render('admin/table-info-loker', {
                    role: session_store.status,
                    result: result
                })
            })
    },
    getEditLoker: (req, res, next) => {
        database.selectWhere('tbl_info', '*', {
                id: req.params.id
            })
            .then(result => {
                res.render('admin/form-edit-loker', {
                    role: session_store.status,
                    data: result[0],
                    error: error
                })
                error = null
            })
    },
    postEditLoker: (req, res, next) => {
        let data = {
            title: req.body.title,
            deskripsi: req.body.deskripsi
        }

        database.update('tbl_info', data, {
                id: req.params.id
            })
            .then(result => {
                if (result) {
                    error = 2
                    res.redirect(`/users/edit.loker/${req.params.id}`)
                } else {
                    error = 2
                    res.redirect(`/users/edit.loker/${req.params.id}`)
                }
            })
    },
    postDelLoker: (req, res, next) => {
        database.delete('tbl_info', {
                id: req.body.id
            })
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },


    getInfoBeasiswa: (req, res, next) => {
        res.render('admin/form-info-beasiswa', {
            role: session_store.status,
            error: error
        })
        error = null
    },
    postInfoBeasiswa: (req, res, next) => {
        let data = {
            title: req.body.title,
            deskripsi: req.body.deskripsi,
            status: 2,
            foto: `/admin/images/foto-beasiswa/${req.file.filename}`
        }

        database.insert('tbl_info', data)
            .then(result => {
                if (result.status) {
                    error = 2
                    res.redirect('/users/form-info-beasiswa')
                } else {
                    error = 1
                    res.redirect('/users/form-info-beasiswa')
                }
            })
    },
    getTableBeasiswa: (req, res, next) => {
        database.selectWhere('tbl_info', '*', {
                status: 2 
            })
            .then(result => {
                res.render('admin/table-info-beasiswa', {
                    role: session_store.status,
                    result: result
                })
            })
    },
    getEditBeasiswa: (req, res, next) => {
        database.selectWhere('tbl_info', '*', {
                id: req.params.id
            })
            .then(result => {
                res.render('admin/form-edit-beasiswa', {
                    role: session_store.status,
                    data: result[0],
                    error: error
                })
                error = null
            })
    },
    postEditBeasiswa: (req, res, next) => {
        let data = {
            title: req.body.title,
            deskripsi: req.body.deskripsi
        }

        database.update('tbl_info', data, {
                id: req.params.id
            })
            .then(result => {
                if (result) {
                    error = 2
                    res.redirect(`/users/edit.beasiswa/${req.params.id}`)
                } else {
                    error = 2
                    res.redirect(`/users/edit.beasiswa/${req.params.id}`)
                }
            })
    },
    postDelBeasiswa: (req, res, next) => {
        database.delete('tbl_info', {
                id: req.body.id
            })
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },

    getPrestasiSekolah: (req, res, next) => {
        res.render('admin/form-prestasi-sekolah', {
            role: session_store.status,
            error: error
        })
        error = null
    },
    postPrestasiSekolah: (req, res, next) => {
        let tgl = req.body.tgl.split('/')


        let data = {
            title: req.body.title,
            deskripsi: req.body.deskripsi,
            foto: `/admin/images/foto-info/${req.file.filename}`,
            tgl: `${tgl[2]}-${tgl[0]}-${tgl[1]}`,
            status: 1
        }

        database.insert('tbl_kegiatan_sekolah', data)
            .then(result => {
                if (result.status) {
                    error = 2
                    res.redirect('/users/form-prestasi-sekolah')
                } else {
                    error = 1
                    res.redirect('/users/form-prestasi-sekolah')
                }
            })
    },
    getTablePrestasi: (req, res, next) => {
        database.selectWhere('tbl_kegiatan_sekolah', `*, DATE_FORMAT(tgl, '%d-%m-%Y') AS date`, {
                status: 1
            })
            .then(result => {
                res.render('admin/table-prestasi-sekolah', {
                    role: session_store.status,
                    result: result
                })
            })
    },
    getEditPrestasi: (req, res, next) => {
        database.selectWhere('tbl_kegiatan_sekolah', `*, DATE_FORMAT(tgl, '%m/%d/%Y') AS date`, {
                id: req.params.id
            })
            .then(result => {
                res.render('admin/form-edit-prestasi', {
                    role: session_store.status,
                    data: result[0],
                    error: error
                })
                error = null
            })
    },
    postEditPrestasi: (req, res, next) => {
        let tgl = req.body.tgl.split('/')
        let data = {
            title: req.body.title,
            deskripsi: req.body.deskripsi,
            tgl: `${tgl[2]}-${tgl[0]}-${tgl[1]}`
        }

        database.update('tbl_kegiatan_sekolah', data, {
                id: req.params.id
            })
            .then(result => {
                if (result) {
                    error = 2
                    res.redirect(`/users/edit.prestasi/${req.params.id}`)
                } else {
                    error = 2
                    res.redirect(`/users/edit.prestasi/${req.params.id}`)
                }
            })
    },
    postDelPrestasi: (req, res, next) => {
        database.delete('tbl_kegiatan_sekolah', {
                id: req.body.id
            })
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },

    getBeritaSekolah: (req, res, next) => {
        res.render('admin/form-berita', {
            role: session_store.status,
            error: error
        })
        error = null
    },
    PostBeritaSekolah: (req, res, next) => {
        let tgl = req.body.tgl.split('/')


        let data = {
            title: req.body.title,
            deskripsi: req.body.deskripsi,
            foto: `/admin/images/foto-info/${req.file.filename}`,
            tgl: `${tgl[2]}-${tgl[0]}-${tgl[1]}`,
            status: 2
        }

        database.insert('tbl_kegiatan_sekolah', data)
            .then(result => {
                if (result.status) {
                    error = 2
                    res.redirect('/users/form-berita-sekolah')
                } else {
                    error = 1
                    res.redirect('/users/form-berita-sekolah')
                }
            })
    },
    getTableBeritaSekolah: (req, res, next) => {
        database.selectWhere('tbl_kegiatan_sekolah', `*, DATE_FORMAT(tgl, '%d-%m-%Y') AS date`, {
                status: 2
            })
            .then(result => {
                res.render('admin/table-berita', {
                    role: session_store.status,
                    result: result
                })
            })
    },
    getEditBeritaSekolah: (req, res, next) => {
        database.selectWhere('tbl_kegiatan_sekolah', `*, DATE_FORMAT(tgl, '%m/%d/%Y') AS date`, {
                id: req.params.id
            })
            .then(result => {
                res.render('admin/form-edit-berita', {
                    role: session_store.status,
                    data: result[0],
                    error: error
                })
                error = null
            })
    },
    postEditBeritaSekolah: (req, res, next) => {
        let tgl = req.body.tgl.split('/')
        let data = {
            title: req.body.title,
            deskripsi: req.body.deskripsi,
            tgl: `${tgl[2]}-${tgl[0]}-${tgl[1]}`
        }

        database.update('tbl_kegiatan_sekolah', data, {
                id: req.params.id
            })
            .then(result => {
                if (result) {
                    error = 2
                    res.redirect(`/users/edit.berita/${req.params.id}`)
                } else {
                    error = 2
                    res.redirect(`/users/edit.berita/${req.params.id}`)
                }
            })
    },
    postDelBeritaSekolah: (req, res, next) => {
        database.delete('tbl_kegiatan_sekolah', {
                id: req.body.id
            })
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },
    postUpdateSemester: async (req, res, next) => {
        let id = req.body.id
        if (id == 1) {
            let ganjil = await database.update('tbl_semester', {
                status: 1
            }, {
                id: 1
            })
            let genap = await database.update('tbl_semester', {
                status: 0
            }, {
                id: 2
            })
            if (ganjil == true && genap == true) {
                res.send(true)
            } else {
                res.send(false)
            }
        } else if(id == 2){
            let ganjil = await database.update('tbl_semester', {
                status: 0
            }, {
                id: 1 
            })
            let genap = await database.update('tbl_semester', {
                status: 1
            }, {
                id: 2
            })
            if (ganjil == true && genap == true) {
                res.send(true)
            } else {
                res.send(false)
            }
        }
    },
    getUpdateSemester : (req, res, next) =>{
        database.select('tbl_semester', '*')
            .then(result => {
               
                
                res.render('admin/form-update-semester',{
                    role : session_store.status,
                    result : result
                })
            })
    },
    getChangePassword : (req, res, next) => {
        res.render('admin/form-password',{
            role : session_store.status,
            error : error
        }) 
        error = null
    },
    postChangePassword : (req, res, next) =>{
        let password = passwordHash.generate(req.body.password)

        

        database.update('tbl_admin',{password : password},{id : session_store.id_user})
            .then(result =>{
                if (result) {
                    error = 2
                    res.redirect('/users/change.password')
                } else {
                    error = 1
                    res.redirect('/users/change.password')
                }
            })  
    },

    getFormStruktur : (req, res, next) =>{
        database.select('tbl_struktur_organisasi', '*')
            .then(result =>{
                res.render('admin/form-struktur',{
                    role : session_store.status,
                    result : result,
                    error : error
                })
                error = null
            })
    },

    postFormStruktur : async (req, res, next) => {
        let data = {
            id_struktur : req.body.id_struktur,
            nama : req.body.nama,
            periode : req.body.periode,
            foto : `/admin/images/foto-struktur/${req.file.filename}`,
            status : 1
        }
       await database.update('tbl_organisasi', {status : 0}, {id_struktur : data.id_struktur});
        database.insert('tbl_organisasi', data)
            .then(result =>{
                if (result.status) {
                    error = 2
                    res.redirect('/users/form-struktur')
                } else {
                    error = 1
                    res.redirect('/users/form-struktur')
                }
            })
    },
    getEditStruktur : async (req, res, next) =>{
        let struktur = await database.select('tbl_struktur_organisasi', '*')
        let data = await database.selectWhere('tbl_organisasi', '*', {id : req.params.id})
        res.render('admin/form-edit-struktur', {
            role : session_store.status,
            result: struktur,
            data : data[0]
        })
    },
    postEditStruktur : async (req, res, next) => {
        let data = {
            id_struktur : req.body.id_struktur,
            nama : req.body.nama,
            periode : req.body.periode,
         
          
        }
        database.update('tbl_organisasi', data, {id : req.params.id})
            .then(result =>{
                if (result) {
                    res.redirect('/users/table-struktur')
                } else {
                    res.redirect('/users/table-struktur')
                }
            })

       
    },
    getTableStruktur : (req, res, next) =>{
        database.query('SELECT o.*, s.posisi FROM tbl_organisasi o, tbl_struktur_organisasi s WHERE o.id_struktur = s.id')
            .then(result =>{
                res.render('admin/table-struktur',{
                    role : session_store.status,
                    result : result
                })
            })
    },
    postDelStruktur : (req, res, next) =>{
        database.delete('tbl_organisasi', {id : req.body.id})
            .then(result => {
                return result ? res.send(true) : res.send(false)
            })
    },
    getLogout: (req,res, next) =>{
        req.session.destroy(function(err) {
            if (err) {
                throw err
            }
            res.redirect('/users/login')
          })
    },
    getKategoriEkskul : (req, res, next) =>{
        res.render('admin/form-ekskul',{
            role : session_store.status,
            error : error
        })
        error = null
    },
    postKategoriEkskul : (req, res, next) =>{
        let  data = {
            kategori : req.body.kategori
        }

        database.insert('tbl_ekskul',data)
            .then(result => {
                if (result.status) {
                    error = 2
                    res.redirect('/users/form-ekskul')
                } else {
                    error = 1
                    res.redirect('/users/form-ekskul')
                }
            })
    },
    getEditEkskul : (req, res, next) =>{
        database.selectWhere('tbl_ekskul', '*', {id : req.params.id})
            .then(result =>{
                res.render('admin/form-edit-ekskul',{
                    data : result[0],
                    error : error,
                    role : session_store.status
                })
                error = null
            })
    },
    postEditEkskul : (req, res, next) => {
        database.update('tbl_ekskul',{kategori : req.body.kategori}, {id : req.params.id})
            .then(result =>{
                if (result) {
                    error = 2
                    res.redirect(`/users/edit.ekskul/${req.params.id}`)
                } else {
                    error = 2
                    res.redirect(`/users/edit.ekskul/${req.params.id}`)
                }
            })
    },
    getTableEkskul : (req, res, next) =>{
        database.select('tbl_ekskul', '*')
            .then(result =>{
                res.render('admin/table-ekskul', {
                    result : result,
                     role  : session_store.status
                })
            })
    },
    getTableAnggotaEkskul : (req, res, next) =>{
        database.query(`SELECT s.nama, a.* FROM tbl_siswa s, tbl_anggota_ekskul a WHERE a.id_siswa = s.id AND id_ekskul = ${req.params.id}`)
            .then(result =>{
                res.render('admin/tbl-anggota-ekskul', {
                    role : session_store.status,
                    result : result,
                    id : req.params.id
                })
            })
    },
    getTableDaftarEkskul : async (req, res, next) =>{
        
        database.query(`SELECT * FROM tbl_siswa WHERE id NOT IN (SELECT id_siswa FROM tbl_anggota_ekskul WHERE id_ekskul = ${req.params.id})`)
            .then(result => {
                res.render('admin/table-daftar-anggota',{
                    role : session_store.status,
                    result : result 
                })
            })
        
    },
    postDaftarEkskul : (req, res, next) =>{
        let data = req.body.id
        let id = req.params.id
        if (Array.isArray(data)) {
            data.forEach(function(item){
                database.insert('tbl_anggota_ekskul',{
                    id_siswa : item,
                    id_ekskul : id
                })
            })
           
            
        } else {
            database.insert('tbl_anggota_ekskul',{
                id_siswa : data,
                id_ekskul : id
            })
           
        }
        res.redirect(`/users//table-anggota-ekskul/${id}`)
        
    },
    postDelAnggota : (req, res, next) =>{
        database.delete('tbl_anggota_ekskul', {id : req.body.id})
            .then(result =>{
                return result ? res.send(true) : res.send(false)
            })
    }

}