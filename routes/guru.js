var express = require('express');
var router = express.Router();
let guruController = require('../controllers/GuruController')
let auth = require('../middleware/auth')
/* GET home page. */
router.get('/',auth.checkLoginGuru, guruController.getIndex);
router.get('/login', guruController.getLogin)
router.post('/login',guruController.postLogin)

router.get('/table-kelas-nilai',auth.checkLoginGuru, guruController.getKelasNilai);
router.get('/nilai-kelas/:id_kelas/:id_mapel',auth.checkLoginGuru, guruController.getNilaiKelas)
router.post('/nilai-kelas/:id_kelas/:id_mapel', guruController.postNilaiKelas)

router.get('/table-kelas-kehadiran',auth.checkLoginGuru, guruController.getTableKelasKehadiran)
router.get('/kehadiran-kelas/:id_kelas/:id_mapel',auth.checkLoginGuru, guruController.getKehadiranSiswa)
router.post('/kehadiran-kelas/:id_kelas/:id_mapel',auth.checkLoginGuru, guruController.postKehadiranSiswa)
router.get('/guru.logout', auth.checkLoginGuru, guruController.getLogout)


router.get('/change.password',auth.checkLoginGuru, guruController.getChangePassword)
router.post('/change.password',auth.checkLoginGuru, guruController.postChangePassword)
module.exports = router;
