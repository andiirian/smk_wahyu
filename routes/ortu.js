var express = require('express');
var router = express.Router();
let ortuController = require('../controllers/OrtuController')
let auth = require('../middleware/auth')
/* GET home page. */
router.get('/login', ortuController.getLogin)
router.post('/login', ortuController.postLogin)
router.get('/',auth.checkLoginOrtu, ortuController.getIndex);
router.get('/table-kelas-nilai',auth.checkLoginOrtu, ortuController.getTableKelasNilai);
router.get('/table-nilai/:id',auth.checkLoginOrtu, ortuController.getTableNilai);


router.get('/table-kelas-kehadiran',auth.checkLoginOrtu, ortuController.getTableKelasKehadiran)
router.get('/table-kehadiran/:id',auth.checkLoginOrtu, ortuController.getTableKehadiran);
router.get('/info-beasiswa',auth.checkLoginOrtu, ortuController.getInfoBeasiswa);
router.get('/info-detail-beasiswa/:id', auth.checkLoginOrtu, ortuController.getDetailInfoBeasiswa)
router.get('/info-loker',auth.checkLoginOrtu, ortuController.getInfoLoker);
router.get('/info-detail-loker/:id', auth.checkLoginOrtu, ortuController.getDetailInfoLoker)
router.get('/ortu.logout', auth.checkLoginOrtu, ortuController.getLogout)


router.get('/change.password',auth.checkLoginOrtu, ortuController.getChangePassword)
router.post('/change.password',auth.checkLoginOrtu, ortuController.postChangePassword)
module.exports = router;
