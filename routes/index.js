var express = require('express');
var router = express.Router();
let indexController = require('../controllers/IndexController')

/* GET home page. */
router.get('/', indexController.getIndex);
router.get('/blog/:id', indexController.getBlog)
router.get('/blog-detail/:id', indexController.getBlogDetail)
router.get('/jadwal/:id', indexController.getJadwal)
module.exports = router;
