var express = require('express');
var router = express.Router();
let adminController = require('../controllers/AdminController')
let auth = require('../middleware/auth')
let multer = require('multer')
let path = require('path')
const STORAGE = multer.diskStorage({
    destination: path.join(__dirname + './../public/admin/images/foto-sarana/'),
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
const UPLOADSARANA = multer({
    storage: STORAGE
});
const STORAGE1 = multer.diskStorage({
    destination: path.join(__dirname + './../public/admin/images/foto-loker/'),
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
const UPLOADLOKER = multer({
    storage: STORAGE1
});

const STORAGE2 = multer.diskStorage({
    destination: path.join(__dirname + './../public/admin/images/foto-beasiswa/'),
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
const UPLOADBEASISWA  = multer({
    storage: STORAGE2
});

const STORAGE3 = multer.diskStorage({
    destination: path.join(__dirname + './../public/admin/images/foto-info/'),
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
const UPLOADINFO  = multer({
    storage: STORAGE3
});
const STORAGE4 = multer.diskStorage({
    destination: path.join(__dirname + './../public/admin/images/foto-struktur/'),
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
const UPLOADSTRUKTUR  = multer({
    storage: STORAGE4
});
/* GET users listing. */
router.get('/login', adminController.getLogin)
router.post('/login', adminController.postLogin)
router.get('/', auth.checkLoginAdmin, adminController.getIndex)
router.get('/form-admin', auth.checkLoginAdmin,adminController.getFormAdmin)
router.post('/form-admin', auth.checkLoginAdmin,adminController.postFormAdmin)

router.get('/form-mapel', auth.checkLoginAdmin,adminController.getFormMapel)
router.post('/form-mapel', auth.checkLoginAdmin,adminController.postFormMapel)
router.get('/table-mapel', auth.checkLoginAdmin,adminController.getTableMapel)
router.get('/edit.mapel/:id', auth.checkLoginAdmin,adminController.getEditMapel)
router.post('/edit.mapel/:id', auth.checkLoginAdmin,adminController.postEditMapel)
router.post('/del.mapel', auth.checkLoginAdmin,adminController.postDelMapel)

router.get('/form-guru', auth.checkLoginAdmin,adminController.getFormGuru)
router.post('/form-guru', auth.checkLoginAdmin,adminController.postFormGuru)
router.get('/table-guru', auth.checkLoginAdmin,adminController.getTableGuru)
router.get('/edit.guru/:id', auth.checkLoginAdmin,adminController.getEditFormGuru)
router.post('/edit.guru/:id', auth.checkLoginAdmin,adminController.postEditFormGuru)
router.post('/del.guru', auth.checkLoginAdmin,adminController.postDelGuru)


router.get('/form-kelas', auth.checkLoginAdmin,adminController.getFormKelas)
router.post('/form-kelas', auth.checkLoginAdmin,adminController.postFormKelas)
router.get('/table-kelas', auth.checkLoginAdmin,adminController.getDatabaseKelas)
router.get('/edit.kelas/:id', auth.checkLoginAdmin,adminController.getFormEditKelas)
router.post('/edit.kelas/:id', auth.checkLoginAdmin,adminController.postFormEditKelas)
router.post('/del.kelas', auth.checkLoginAdmin,adminController.postDelKelas)


router.get('/form-ta', auth.checkLoginAdmin,adminController.getFormTa)
router.post('/form-ta', auth.checkLoginAdmin,adminController.postFormTa)
router.get('/table-ta', auth.checkLoginAdmin,adminController.getTableTa)
router.get('/edit.ta/:id', auth.checkLoginAdmin,adminController.getEditFormTa)
router.post('/edit.ta/:id', auth.checkLoginAdmin,adminController.postEditFormTa)
router.post('/del.ta', auth.checkLoginAdmin,adminController.postDelTa)


router.get('/form-siswa', auth.checkLoginAdmin,adminController.getFormSiswa)
router.post('/form-siswa',auth.checkLoginAdmin, adminController.postFormSiswa)
router.get('/table-siswa',auth.checkLoginAdmin, adminController.getTableSiswa)
router.get('/edit.siswa/:id', auth.checkLoginAdmin,adminController.getEditFormSiswa)
router.post('/edit.siswa/:id',auth.checkLoginAdmin, adminController.postEditFormSiswa)
router.post('/del.siswa',auth.checkLoginAdmin, adminController.postDelSiswa)
router.get('/table-siswa-tamat', auth.checkLoginAdmin, adminController.getTableSiswaTamat)


router.get('/table-kurikulum',auth.checkLoginAdmin,adminController.getTableKur)
router.get('/table-kurikulum/:id', auth.checkLoginAdmin,adminController.getTableKelas)
router.get('/table-kurikulum-kelas/:id/:kelas', auth.checkLoginAdmin,adminController.getTableKurKelas)
router.get('/form-kurikulum/:id/:kelas', auth.checkLoginAdmin,adminController.getFormKurikulum)
router.post('/form-kurikulum/:id/:kelas',auth.checkLoginAdmin, adminController.postFormKurikulum)
router.get('/edit.kurikulum/:id_semester/:kelas/:id',auth.checkLoginAdmin, adminController.getEditFormKurikulum)
router.post('/edit.kurikulum/:id_semester/:kelas/:id', auth.checkLoginAdmin,adminController.postEditFormKurikulum)
router.post('/del.kurikulum', auth.checkLoginAdmin,adminController.postDelKurikulum)
router.post('/select.guru', auth.checkLoginAdmin, adminController.postSelectGuru)


router.get('/table-kelas-siswa', auth.checkLoginAdmin,adminController.getTableKelasSiswa)
router.post('/table-kelas-siswa', auth.checkLoginAdmin,adminController.postTableKelasSiswa)

router.get('/table-pkl',auth.checkLoginAdmin, adminController.getTablePkl)
router.get('/form-pkl/:id',auth.checkLoginAdmin, adminController.getFormPkl)
router.post('/form-pkl/:id', auth.checkLoginAdmin,adminController.postFormPkl)


router.get('/form-visimisi',auth.checkLoginAdmin, adminController.getFormVisi)
router.post('/form-visimisi',auth.checkLoginAdmin, adminController.postFormVisi)
router.get('/edit.visimisi/:id', auth.checkLoginAdmin, adminController.getEditFormVisi)
router.post('/edit.visimisi/:id', auth.checkLoginAdmin, adminController.postEditFormVisi)

router.get('/form-profil-sekolah', auth.checkLoginAdmin, adminController.getFormProfil)
router.post('/form-profil-sekolah', auth.checkLoginAdmin, adminController.postFormProfil)
router.get('/edit.profilsekolah/:id', auth.checkLoginAdmin, adminController.getEditFormProfil)
router.post('/edit.profilsekolah/:id', auth.checkLoginAdmin, adminController.postEditFormProfil)


router.get('/form-kalender-pendidikan', auth.checkLoginAdmin, adminController.getFormKalender)
router.post('/form-kalender-pendidikan', auth.checkLoginAdmin, adminController.postFormKalender)
router.get('/table-kalender-pendidikan', auth.checkLoginAdmin, adminController.getTableKalender)
router.get('/edit.kalenderpendidikan/:id', auth.checkLoginAdmin, adminController.getFormEditKalender)
router.post('/edit.kalenderpendidikan/:id', auth.checkLoginAdmin, adminController.postFormEditKalender)
router.post('/del.kalenderpendidikan', auth.checkLoginAdmin, adminController.postDelKalender)


router.get('/form-kategori-sarana', auth.checkLoginAdmin,adminController.getFormKategoriSarana)
router.post('/form-kategori-sarana', auth.checkLoginAdmin,UPLOADSARANA.array('foto-sarana'),adminController.postFormKategoriSarana)
router.get('/edit.kategorisarana/:id', auth.checkLoginAdmin,adminController.getEditKategoriSarana)
router.get('/table-kategori-sarana', auth.checkLoginAdmin,adminController.getTableKategoriSarana)
router.get('/lihat.fotosarana/:id', auth.checkLoginAdmin, adminController.getFotoSarana)
router.post('/edit.kategorisarana/:id', auth.checkLoginAdmin,adminController.postEditKategoriSarana)
router.post('/del.kategorisarana', auth.checkLoginAdmin,adminController.postDelKategoriSarana)
router.post('/del.fotosarana', auth.checkLoginAdmin,adminController.delFotoSarana)
router.get('/tambah.fotosarana/:id', auth.checkLoginAdmin, adminController.getFormFotoSarana)
router.post('/tambah.fotosarana/:id', auth.checkLoginAdmin, UPLOADSARANA.array('foto-sarana'), adminController.postFotoSarana)



router.get('/form-info-loker', auth.checkLoginAdmin,adminController.getInfoLoker)
router.post('/form-info-loker', auth.checkLoginAdmin,UPLOADLOKER.single('foto-loker'),adminController.postInfoloker)
router.get('/table-info-loker', auth.checkLoginAdmin,adminController.getTableLoker)
router.get('/edit.loker/:id', auth.checkLoginAdmin,adminController.getEditLoker)
router.post('/edit.loker/:id', auth.checkLoginAdmin,adminController.postEditLoker)
router.post('/del.loker', auth.checkLoginAdmin,adminController.postDelLoker)


router.get('/form-info-beasiswa', auth.checkLoginAdmin,adminController.getInfoBeasiswa)
router.post('/form-info-beasiswa', auth.checkLoginAdmin,UPLOADBEASISWA.single('foto-beasiswa'),adminController.postInfoBeasiswa)
router.get('/table-info-beasiswa', auth.checkLoginAdmin,adminController.getTableBeasiswa)
router.get('/edit.beasiswa/:id', auth.checkLoginAdmin,adminController.getEditBeasiswa)
router.post('/edit.beasiswa/:id', auth.checkLoginAdmin,adminController.postEditBeasiswa)
router.post('/del.beasiswa', auth.checkLoginAdmin,adminController.postDelBeasiswa)



router.get('/form-prestasi-sekolah', auth.checkLoginAdmin,adminController.getPrestasiSekolah)
router.post('/form-prestasi-sekolah', auth.checkLoginAdmin,UPLOADINFO.single('foto-prestasi'),adminController.postPrestasiSekolah)
router.get('/table-prestasi-sekolah', auth.checkLoginAdmin,adminController.getTablePrestasi)
router.get('/edit.prestasi/:id', auth.checkLoginAdmin,adminController.getEditPrestasi)
router.post('/edit.prestasi/:id', auth.checkLoginAdmin,adminController.postEditPrestasi)
router.post('/del.prestasi', auth.checkLoginAdmin,adminController.postDelPrestasi)


router.get('/form-berita-sekolah', auth.checkLoginAdmin,adminController.getBeritaSekolah)
router.post('/form-berita-sekolah', auth.checkLoginAdmin,UPLOADINFO.single('foto-berita'),adminController.PostBeritaSekolah)
router.get('/table-berita-sekolah', auth.checkLoginAdmin,adminController.getTableBeritaSekolah)
router.get('/edit.berita/:id', auth.checkLoginAdmin,adminController.getEditBeritaSekolah)
router.post('/edit.berita/:id', auth.checkLoginAdmin,adminController.postEditBeritaSekolah)
router.post('/del.berita', auth.checkLoginAdmin,adminController.postDelBeritaSekolah)


router.get('/form-update-semester', auth.checkLoginAdmin, adminController.getUpdateSemester)
router.post('/update.semester', auth.checkLoginAdmin, adminController.postUpdateSemester)


router.get('/change.password',auth.checkLoginAdmin, adminController.getChangePassword)
router.post('/change.password',auth.checkLoginAdmin, adminController.postChangePassword)


router.get('/form-struktur',auth.checkLoginAdmin, adminController.getFormStruktur)
router.post('/form-struktur',auth.checkLoginAdmin,UPLOADSTRUKTUR.single('foto-struktur'), adminController.postFormStruktur)
router.get('/table-struktur', auth.checkLoginAdmin, adminController.getTableStruktur)
router.get('/edit.struktur/:id', auth.checkLoginAdmin, adminController.getEditStruktur)
router.post('/edit.struktur/:id', auth.checkLoginAdmin, adminController.postEditStruktur)
router.post('/del.struktur', auth.checkLoginAdmin, adminController.postDelStruktur)


router.get('/form-ekskul', auth.checkLoginAdmin, adminController.getKategoriEkskul)
router.post('/form-ekskul', auth.checkLoginAdmin, adminController.postKategoriEkskul)
router.get('/table-ekskul', auth.checkLoginAdmin, adminController.getTableEkskul)
router.get('/edit.ekskul/:id', auth.checkLoginAdmin, adminController.getEditEkskul)
router.post('/edit.ekskul/:id', auth.checkLoginAdmin, adminController.postEditEkskul)
router.get('/table-anggota-ekskul/:id', auth.checkLoginAdmin, adminController.getTableAnggotaEkskul)
router.get('/tambah.anggota/:id', auth.checkLoginAdmin, adminController.getTableDaftarEkskul)
router.post('/tambah.anggota/:id', auth.checkLoginAdmin, adminController.postDaftarEkskul)
router.post('/del.anggota', auth.checkLoginAdmin, adminController.postDelAnggota )



router.get('/user.logout', auth.checkLoginAdmin, adminController.getLogout)
module.exports = router;
