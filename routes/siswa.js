var express = require('express');
var router = express.Router();
let siswaController = require('../controllers/SiswaController')
let auth = require('../middleware/auth')
/* GET home page. */
router.get('/login', siswaController.getLogin)
router.post('/login', siswaController.postLogin)
router.get('/',auth.checkLoginSiswa, siswaController.getIndex);
router.get('/table-kelas-nilai',auth.checkLoginSiswa, siswaController.getTableKelasNilai);
router.get('/table-nilai/:id',auth.checkLoginSiswa, siswaController.getTableNilai);


router.get('/table-kelas-kehadiran',auth.checkLoginSiswa, siswaController.getTableKelasKehadiran)
router.get('/table-kehadiran/:id',auth.checkLoginSiswa, siswaController.getTableKehadiran);
router.get('/siswa.logout', auth.checkLoginSiswa, siswaController.getLogout)

router.get('/change.password',auth.checkLoginSiswa, siswaController.getChangePassword)
router.post('/change.password',auth.checkLoginSiswa, siswaController.postChangePassword)

router.get('/table-nilai-pkl', auth.checkLoginSiswa, siswaController.getNilaiPkl)
module.exports = router;
