module.exports = {
    checkLoginAdmin : (req, res, next) => {
        if (req.session.logged_in == true && req.session.status == 1) {
            next()
        }else{ 
            res.redirect('/users/login')
        }
    },
    checkLoginSiswa : (req , res, next) => {
        if (req.session.logged_in == true && req.session.status == 2) {
            next()
        }else{ 
            res.redirect('/siswa/login')
        }
    },
    checkLoginGuru : (req , res, next) => {
        if (req.session.logged_in == true && req.session.status == 3) {
            next()
        }else{ 
            res.redirect('/guru/login')
        }
    },
    checkLoginOrtu : (req , res, next) => {
        if (req.session.logged_in == true && req.session.status == 4) {
            next()
        }else{ 
            res.redirect('/ortu/login')
        }
    }
}