const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'smk_wahyu'
});
module.exports = {
  insert: (table, data) => {
    return new Promise(resolve => {
      connection.query(`INSERT INTO ${table} set ? `, data, (err, result) => {
        return err ? resolve(err) : resolve({
          status: true,
          result: result
        })

      })
    })
  },
  select: (table, data) => {
    return new Promise(resolve => {
      connection.query(`SELECT ${data} FROM ${table}`, (err, result) => {
        return err ? resolve(false) : resolve(result)
      })
    })
  },
  selectAllDescLimit: (table, column, limit) => {
    return new Promise(resolve => {
      connection.query(`SELECT * FROM ${table} ORDER BY ${column} 
        DESC LIMIT ${limit}`, (err, result) => {
        return err ? resolve(false) : resolve(result);
      })
    })
  },
  query: (query) => {
    return new Promise(resolve => {
      connection.query(query, (err, result) => {
        return err ? resolve(false) : resolve(result);
      })
    })
  },
  selectWhere: (table, data, kondisi) => {
    return new Promise(resolve => {
      connection.query(`SELECT ${data} FROM ${table} WHERE ?`,
        kondisi, (err, result) => {
          return err ? resolve(err) : resolve(result)
        })
    })
  },
  selectAllDesc: (table, column) => {
    return new Promise(resolve => {
      connection.query(`SELECT * FROM ${table} ORDER BY ${column}`,
        (err, result) => {
          return err ? resolve(false) : resolve(result);
        })
    })
  },
  selectCount: (table, column, as) => {
    return new Promise(resolve => {
      connection.query(`SELECT COUNT(${column}) AS ${as} FROM ${table}`,
        (err, result) => {
          return err ? resolve(false) : resolve(result)

        })
    })
  },
  selectLastId: () => {
    return new Promise(resolve => {
      connection.query('SELECT LAST_INSERT_ID() AS id',
        (err, result) => {
          return err ? resolve(false) : resolve(result);
        })
    })
  },
  selectCountWhere: (table, column, as, kondisi) => {
    return new Promise(resolve => {
      connection.query(`SELECT COUNT(${column}) AS ${as}
       FROM ${table} WHERE ?`, kondisi, (err, result) => {
        return err ? resolve(false) : resolve(result)
      })
    })
  },
  selectCountWhereAnd: (table, column, as, kondisi) => {
    return new Promise(resolve => {
      connection.query(`SELECT COUNT(${column}) AS ${as}
       FROM ${table} WHERE ? AND ?`, kondisi, (err, result) => {
        return err ? resolve(false) : resolve(result)
      })
    }) 
  }, 
  selectWhereAnd: (table, data, kondisi) => {
    return new Promise(resolve => {
      connection.query(`SELECT ${data} FROM ${table} WHERE ? AND ? `,
        kondisi, (err, result) => {
          return err ? resolve(err) : resolve(result)
        })
    })
  },
  delete: (table, kondisi) => {
    return new Promise(resolve => {
      connection.query(`DELETE FROM ${table} WHERE ? `, kondisi, (err => {
        return err ? resolve(err) : resolve(true)
      }))
    })
  },
  update: (table, data, kondisi) => {
    return new Promise(resolve => {
      connection.query(`UPDATE ${table} set ? WHERE ?`, [data, kondisi], (err => {
        return err ? resolve(false) : resolve(true)
      }))
    })
  },
  deleteAnd: (table, kondisi) => {
    return new Promise(resolve => {
      connection.query(`DELETE FROM ${table} WHERE ? AND ?`, kondisi, (err => {
        return err ? resolve(false) : resolve(true)
      }))
    })
  },
   selectWith : (table, data, kondisi) =>{
     return new Promise (resolve =>{
       connection.query(`SELECT ${data} FROM ${table} WHERE ${kondisi}`, (err, result) =>{
         return err ? resolve({status : false}) : resolve({status : true, result : result});
       })
     })
   },
   selectJoin : (sql) => {
    return new Promise (resolve =>{
      connection.query(`{sql: ${sql}, nestTables: '_'};`, (err, result) =>{
        return err ? resolve({status : false}) : resolve(result);
      })
    })
   }
}